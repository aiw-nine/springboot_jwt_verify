package com.aiw.springboot_jwt_verify.controller;

import com.aiw.springboot_jwt_verify.entity.User;
import com.aiw.springboot_jwt_verify.response.R;
import com.aiw.springboot_jwt_verify.service.UserService;
import com.aiw.springboot_jwt_verify.utils.JwtUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 登录，此处只做简单测试
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public R<Map> login(@RequestBody User user) {
        // 进行数据库查询
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getName, user.getName()).eq(User::getPwd, user.getPwd());
        User one = userService.getOne(wrapper);
        if (Objects.nonNull(one)) {
            // 登录成功，根据用户id生成token并返回登录成功结果
            Map<String, Object> map = new HashMap<>();
            map.put("user", one);
            map.put("token", JwtUtil.sign(one.getId()));
            return R.success("登录成功", map);
        }
        return R.fail("登录失败");
    }


    /**
     * 此处做测试，看用户在未登录时，能否访问到此接口
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public R<List<User>> index() {
        return R.success("访问成功", userService.list());
    }

}

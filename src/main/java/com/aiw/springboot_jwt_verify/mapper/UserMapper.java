package com.aiw.springboot_jwt_verify.mapper;

import com.aiw.springboot_jwt_verify.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Aiw
* @description 针对表【user】的数据库操作Mapper
* @createDate 2022-08-13 11:45:04
* @Entity com.aiw.springboot_jwt_verify.entity.User
*/
public interface UserMapper extends BaseMapper<User> {

}





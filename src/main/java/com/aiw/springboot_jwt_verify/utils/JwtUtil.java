package com.aiw.springboot_jwt_verify.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

import java.util.Date;

/* *
 * @Author qy
 * <p> JWT工具类 </p>
 * @Param
 * @Return
 */
public class JwtUtil {

    // Token过期时间30分钟
    public static final long EXPIRE_TIME = 30 * 60 * 1000;

    // 密钥盐
    public static final String TOKEN_SECRET = "aiw";

    /**
     * 根据用户id生成签名,30min后过期
     *
     * @param id
     * @return
     */
    public static String sign(Integer id) {
        Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
        Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
        // 附带uid信息
        return JWT.create()
                .withClaim("id", id)
                .withExpiresAt(date)
                .sign(algorithm);
    }


    /**
     * 校验token是否正确
     *
     * @param token
     * @return
     */
    public static boolean verify(String token) {
        try {
            // 设置加密算法
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withClaim("id", JwtUtil.getUidByToken(token))
                    .build();   //创建token验证器
            // 效验TOKEN
            DecodedJWT jwt = verifier.verify(token);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    /**
     * 获取用户Id
     *
     * @return
     */
    public static Integer getUidByToken(String token) {
        DecodedJWT jwt = JWT.decode(token);
        return jwt.getClaim("id").asInt();
    }
}


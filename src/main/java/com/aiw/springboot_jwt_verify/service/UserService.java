package com.aiw.springboot_jwt_verify.service;

import com.aiw.springboot_jwt_verify.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Aiw
* @description 针对表【user】的数据库操作Service
* @createDate 2022-08-13 11:45:04
*/
public interface UserService extends IService<User> {

}

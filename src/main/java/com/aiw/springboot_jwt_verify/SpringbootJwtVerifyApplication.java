package com.aiw.springboot_jwt_verify;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.aiw.springboot_jwt_verify.mapper")
@SpringBootApplication
public class SpringbootJwtVerifyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootJwtVerifyApplication.class, args);
    }

}

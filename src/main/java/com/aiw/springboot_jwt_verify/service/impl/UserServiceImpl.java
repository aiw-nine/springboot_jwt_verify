package com.aiw.springboot_jwt_verify.service.impl;

import com.aiw.springboot_jwt_verify.entity.User;
import com.aiw.springboot_jwt_verify.mapper.UserMapper;
import com.aiw.springboot_jwt_verify.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author Aiw
 * @description 针对表【user】的数据库操作Service实现
 * @createDate 2022-08-13 11:45:04
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
        implements UserService {

}





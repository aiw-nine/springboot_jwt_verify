/*
 Navicat Premium Data Transfer

 Source Server         : Aiw
 Source Server Type    : MySQL
 Source Server Version : 50528
 Source Host           : localhost:3306
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 50528
 File Encoding         : 65001

 Date: 13/08/2022 15:14:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
  `pwd` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
  `age` int(11) NOT NULL,
  `sex` varchar(255) CHARACTER SET gbk COLLATE gbk_chinese_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = gbk COLLATE = gbk_chinese_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '小白', '123', 20, '男');
INSERT INTO `user` VALUES (2, '小羊', '456', 20, '女');
INSERT INTO `user` VALUES (3, 'alpha', '789', 21, '男');
INSERT INTO `user` VALUES (5, 'shawvivi', '666', 18, '未知');
INSERT INTO `user` VALUES (9, '小黑', '777', 20, '未知');
INSERT INTO `user` VALUES (10, '贝塔', '888', 21, '女');
INSERT INTO `user` VALUES (11, '小黄', '333', 22, '女');

SET FOREIGN_KEY_CHECKS = 1;
